---
duration: 40
presentation_url:
room:
slot:
speakers:
- Mahmoud Hashemi
title: "Ask the Ecosystem: Lessons from 200+ FOSS Applications"
type: talk
video_url:
---
If you had to build a software application right now, how would you do it?

First step, Python. But then what?

This talk looks at over 200 of the most-successful open-source Python
applications to distill real-world advice on building effective software to
reach the masses. Architecture, testing, licensing, packaging and
distribution, these projects hold lifetimes of work and wisdom, waiting to
be learned!

In this presentation, we'll explore many questions, just a few of which
include:

* What userbases do Python-based applications reach?
* Where in software is Python leading, and what domains represent its
  biggest gaps?
* What library dependencies appear in the application zeitgeist?
* What copyright licenses are used by applications, and how do these
  practices differ from libraries?
* How many projects rely only on donations, as opposed to having foundation
  or corporation support?
* What can Python developers do to support and get involved in Python's rich
  application space?


A production application is worth a hundred blog posts and a thousand Stack
Overflow answers. You've already heard all the talk about best practices,
now come hear about the practical practices from real Python applications.
