---
duration: 25
presentation_url:
room:
slot:
speakers:
- Gabe Levine
- Jonathan Arfa
title: "You Don\u0027t Need Neural Nets - How Simpler Algorithms Can Solve Your Problems With Less Headache"
type: talk
video_url:
---
You’ve heard a lot about Neural Networks (or “A.I.” as your company’s
marketing team likes to call it), and how they are solution to all of your
problems. Unfortunately, they’re also finicky and complex. And for most
problems that most people deal with, they’re not necessarily much better
than easier-to-use algorithms such as Gradient Boosted Trees (GBTs). We will
explain (briefly) what GBTs are, compare neural networks vs. GBTs on some
benchmark problems, demonstrate where Neural Nets are necessary
(images/sound/text), and share some tips for making GBTs working even on
those cases.

This talk is meant for people with a cursory knowledge of machine learning,
but who aren’t necessarily experts.
