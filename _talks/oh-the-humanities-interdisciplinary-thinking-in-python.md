---
duration: 25
presentation_url:
room:
slot:
speakers:
- Lisa Tagliaferri
title: "Oh, the Humanities! Interdisciplinary Thinking in Python"
type: talk
video_url:
---
The humanities — fields like history, literature, and philosophy — are not
often incorporated into our thought processes in the tech industry. However,
bringing humanities contexts into our everyday work can strengthen the
software we build and ensure that it is serving our communities. Learn about
how creating software for humanities researchers has developed better Python
programmers at MIT, and how our own self-learning can benefit from tackling
projects with humanistic values at their core.

This talk will provide you with an overview of how Python has been used to
help solve humanities research problems, while also demonstrating how
incorporating the humanities into Python development processes can improve
software. We will go over what things you should consider to think in more
interdisciplinary ways, and how broader contexts can ensure that the
software we build serves its users well.
