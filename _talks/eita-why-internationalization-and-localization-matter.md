---
duration: 40
presentation_url:
room:
slot:
speakers:
- Nicolle Cysneiros
title: "Eita! Why Internationalization and Localization matter"
type: talk
video_url:
---
According to the always trustworthy Wikipedia, there are approximately 360
million native English speakers in the world. We, as developers, are so used
to write code and documentation in English that we may not realize that this
number only represents 4.67% of the world population. It is very useful to
have a common language for the communication between developers, but this
doesn’t mean that the user shouldn’t feel a little bit more comfortable when
using your product.

Translation of terms is only one step in the whole Internationalization
(i18n) and Localization (l10n) process. It also entails number, date and
time formatting, currency conversion, sorting, legal requirements, among
other issues. This talk will go through the definition of i18n and l10n as
well as show the main tools available for developers to support multiple
languages and regional related preferences in their Python program. We will
also see how one can enable local support for their website in Django.
Finally, this presentation will discuss how we can manage
Internationalization and Localization for a bigger product running in
different platforms (front and back end) and how to incorporate i18n and
l10n into our current development and deploy processes.

Oh, and by the way, “eita!” is a Brazilian interjection to show yourself
surprised with something.
