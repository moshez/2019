---
duration: 25
presentation_url:
room:
slot:
speakers:
- Moshe Zadka
title: "A Time Traveler\u0027s Perspective on Software Developer Code of Ethics"
type: talk
video_url:
---
In the history part in "Ethics for Software Developer", I have heard that
there was no code of ethics for software developers in the 21st century. Now
that I have time-traveled here from the 23rd century, I realized it was
true. I will share the accepted wisdom from the 23rd century about how code
of ethics and licensing looks like.
