---
duration: 25
presentation_url:
room:
slot:
speakers:
- Ryan Kelly
title: "Advanced SQL with SQLAlchemy"
type: talk
video_url:
---
SQL is an incredibly powerful way to access your data, and SQLAlchemy’s
flexibility allows you to harness all this power in a Pythonic way.
