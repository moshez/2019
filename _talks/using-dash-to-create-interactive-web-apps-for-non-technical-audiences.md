---
duration: 25
presentation_url:
room:
slot:
speakers:
- Joseph Willi
title: "Using Dash to Create Interactive Web Apps for Non-Technical Audiences"
type: talk
video_url:
---
### Overview

Have you ever struggled with finding ways to present data visualizations
and/or results to non-technical audiences in a coherent and engaging manner?
In this talk, I'll detail how I overcame such a challenge by using Dash to
build an interactive app for firefighters to use during performance testing
of their rescue equipment.

### Description

Analytical web applications can serve as a powerful means for scientists and
engineers to interact with data and identify patterns and trends in a
concise and straightforward manner. Such tools can allow users to
immediately see the effects of modifying specific input parameters.
Additionally, interactive web apps can be utilized to present data
visualizations and analysis results in engaging ways.

Unless you're a full-stack developer, creating these types of web
applications may seem quite challenging. Dash, a Python framework written on
top of Flask, Plotly.js, and React.js, handles many of the complexities
associated with building custom interfaces and provides users the ability to
build powerful data visualizations strictly through Python.

Despite being an intermediate Python user lacking full knowledge of the
technologies and protocols required to build web-based applications, I was
able to create a UI using Dash. More specifically, I built an interactive
dashboard for firefighters to process and interact with sensor data
collected during performance testing of their rescue equipment.

During this talk, I will briefly detail the motivation behind this project.
Then, I'll describe how the project progressed to its current state, while
highlighting key points that can be applied to the general case of
developing interactive web apps for audiences from non-technical
backgrounds. To conclude my presentation, I will show a demo of the
interactive web app and summarize the key takeaways.
