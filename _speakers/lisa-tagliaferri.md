---
name: Lisa Tagliaferri
talks:
- "Oh, the Humanities! Interdisciplinary Thinking in Python"
---
Lisa Tagliaferri is on the Developer Educator team at DigitalOcean. A
recovering medievalist with a background in the digital humanities, she has
been a researcher and educator at MIT and the City University of New York.
Her open-access eBook, How To Code in Python, is widely used to teach
Python.
