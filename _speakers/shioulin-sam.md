---
name: Shioulin Sam
talks:
- "Learning with Limited Labeled Data"
---
Shioulin Sam is a research engineer at Cloudera Fast Forward Labs, where she
bridges machine learning academic research and industry applications. In her
previous life, she managed a portfolio of early-stage venture focusing on
women-led startups and public market investments. She also worked in the
investment management industry designing quantitative trading strategies.
She holds a Ph.D in Electrical Engineering and Computer Science from
Massachusetts Institute of Technology.
