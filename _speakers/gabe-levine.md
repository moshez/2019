---
name: Gabe Levine
talks:
- "You Don\u0027t Need Neural Nets - How Simpler Algorithms Can Solve Your Problems With Less Headache"
---
Gabe Levine is a musician turned software engineer obsessed with Machine
Learning and how it can be applied to generating new creative forms,
especially in music. He believes there will be a Grammy for Best AI
Generated Song in our lifetime and founded a [Creative Technology
collective](https://www.standardimpossible.com/) to chase down those ideas.
Currently Gabe works for the [United Technologies Digital
Accelerator](https://digital.utc.com/) in Brooklyn as the Lead Data Engineer
for Pratt & Whitney with a focus on predictive and preventative analytics
for Jet Engines.
