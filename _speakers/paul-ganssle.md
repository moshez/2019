---
name: Paul Ganssle
talks:
- "Build your Python Extensions with Rust!"
---
Paul Ganssle is a software developer, maintainer of python-dateutil and
setuptools and contributor to many other open source projects. He lives in
New York City and is interested in programming, languages, wearable
electronics and sensors.

Expressions of opinion do not necessarily reflect the views of his employer.
