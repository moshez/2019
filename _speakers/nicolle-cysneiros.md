---
name: Nicolle Cysneiros
talks:
- "Eita! Why Internationalization and Localization matter"
---
Fullstack developer @ Labcodes, I work with Python and Django on a daily
basis. I’m part of Python User’s Group and PyLadies in Recife. I’m feminist
and I believe in women’s inclusion and empowerment through technology.
