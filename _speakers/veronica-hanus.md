---
name: Veronica Hanus
talks:
- "To comment or not? A data-driven look at attitudes toward code comments"
---
Before becoming a programmer, Veronica was a researcher with an eye for
process improvement (she helped pick the Mars Curiosity Rover’s landing
site!). She loves exploring the web and teaching, and speaks on building the
tooling & docs you need and has co-taught a PyCon tutorial on using web-
scraping and machine learning to predict Oscar winners. When she isn’t
thinking about how the web can be better for developers, she enjoys blogging
and snuggling as many cats as possible.
