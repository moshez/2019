---
name: Nexmo
tier: gold
site_url: https://developer.nexmo.com/
logo: nexmo.png
twitter: NexmoDev
---
Nexmo is a global cloud communications platform, providing APIs and SDKs for messaging, voice, phone
verification, advanced multi-channel conversations and video calling with the OpenTok API. With our
Messages and Dispatch Beta you can now integrate with various communication channels including
Facebook Messenger, WhatsApp and Viber.
