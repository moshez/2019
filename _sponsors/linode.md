---
name: Linode
tier: gold
site_url: https://www.linode.com/
logo: linode.png
twitter: linode
---
Linode is the leading cloud for developers demanding maximum performance, reliability and value for
their modern applications.

Pioneering cloud hosting since 2003, we empower over 800,000 developers worldwide through our 10
data centers to easily build and scale their server infrastructure.
